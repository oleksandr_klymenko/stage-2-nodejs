const {Validator} = require("./validator");

class FighterValidator extends Validator {
    constructor(res) {
        super(res);
    }

    power(power = 10) {
        const min = 1;
        const max = 100;
        return (Number(power) > min && Number(power) < max) ?
            Number(power) :
            this.setError('Power must be number more than 1 & less than 100');
    }

    defense(defense = 2) {
        const min = 1;
        const max = 10;
        return (Number(defense) > min && Number(defense) < max) ?
            Number(defense) :
            this.setError('Defense must be number more than 1 & less than 10');
    }

    health(health = 100) {
        const min = 80;
        const max = 120;
        return (Number(health) > min && Number(health) < max) ?
            Number(health) :
            this.setError('Health must be number from 0 to 100');
    }

    name(name = '') {
        return name.trim().length >= 3 ? name.trim() : this.setError('Name should contain at least 3 symbols');
    }
}

exports.FighterValidator = FighterValidator;
