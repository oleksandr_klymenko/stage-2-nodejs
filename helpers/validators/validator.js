class Validator {
    constructor(res) {
        this.res = res;
    }

    setError(error) {
        this.res.err = this.res.err ? (`${this.res.err} ${error}.`) : `${error}.`;
        return null;
    }
}

exports.Validator = Validator;
