const {UserRepository} = require('../repositories/userRepository');

class UserService {
    search(search) {
        const user = UserRepository.getOne(search);
        if (!user) {
            return null;
        }
        return user;
    }

    create(userData) {
        const users = UserRepository.getAll();
        if (users.find(item => {
            return item.email.toLowerCase() === userData.email.trim().toLowerCase() ||
                item.phoneNumber === userData.phoneNumber.trim();
        })) {
            throw Error('User already exist');
        }
        const user = UserRepository.create(userData);
        if (!user) {
            throw Error('User entity to create isn\'t valid');
        }
        return user;
    }

    getUser(userId) {
        const user = UserRepository.getOne({id: userId});
        if (!user) {
            throw Error('User not found');
        }
        return user;
    }

    delete(userId) {
        const user = UserRepository.delete(userId);
        if (!user.length) {
            throw Error('User not found');
        }
        return user;
    }

    update(userId, userData) {
        const users = UserRepository.getAll().filter(item => item !== userId);
        if (users.find(item => {
            return item.email.toLowerCase() === userData.email.trim().toLowerCase() ||
                item.phoneNumber === userData.phoneNumber.trim();
        })) {
            throw Error('User already exist');
        }
        const user = UserRepository.update(userId, userData);
        if (!user.id) {
            throw Error('User not found');
        }
        return user;
    }

    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            return null;
        }
        return users;
    }
}

module.exports = new UserService();
