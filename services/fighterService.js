const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {
    getFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters) {
            return null
        }
        return fighters;
    }

    getFighter(fighterId) {
        const fighter = FighterRepository.getOne({id: fighterId});
        if (!fighter) {
            return null
        }
        return fighter;
    }

    create(data) {
        const fighters = FighterRepository.getAll();
        if (fighters.find(item => item.name.toLowerCase() === data.name.trim().toLowerCase())) {
            throw Error('Fighter already exist');
        }
        const fighter = FighterRepository.create(data);
        if (!fighter) {
            return null;
        }
        return fighter;
    }

    update(fighterId, fighterData) {
        const fighters = FighterRepository.getAll().filter(item => item !== fighterId);
        if (fighters.find(item => item.name.toLowerCase() === fighterData.name.trim().toLowerCase())) {
            throw Error('Fighter already exist');
        }
        const fighter = FighterRepository.update(fighterId, fighterData);
        if (!fighter) {
            return null;
        }
        return fighter;
    }

    delete(fighterId) {
        const fighter = FighterRepository.delete(fighterId);
        if (!fighter) {
            return null;
        }
        return fighter;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();
