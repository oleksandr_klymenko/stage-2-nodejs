const {Router} = require('express');
const FighterService = require('../services/fighterService');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {createFighterValid, updateFighterValid} = require('../middlewares/fighter.validation.middleware');

const router = Router();
// get all fighters
router.get('/', (req, res, next) => {
    try {
        res.data = FighterService.getFighters();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)
// get fighter
router.get('/:id', (req, res, next) => {
    try {
        res.data = FighterService.getFighter(req.params.id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)
// create fighter
router.post('/', createFighterValid, (req, res, next) => {
    try {
        if (!res.err) {
            res.data = FighterService.create(req.body);
        }
    } catch (err) {
        res.err = err.message;
    } finally {
        next();
    }
}, responseMiddleware)
// update fighter
router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        if (!res.err) {
            res.data = FighterService.update(req.params.id, req.body)
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)
// delete fighter
router.delete('/:id', (req, res, next) => {
    try {
        res.data = FighterService.delete(req.params.id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)

module.exports = router;
