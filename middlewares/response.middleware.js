const responseMiddleware = (req, res, next) => {
    if (!res.err) {
        res.status(200).json(res.data);
        next();
    } else {
        res.status(400)
            .json({
                error: true,
                message: res.err
            });
    }
}

exports.responseMiddleware = responseMiddleware;
