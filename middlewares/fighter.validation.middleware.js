const {fighter} = require('../models/fighter');
const {permitParams} = require("../helpers/helpers");
const {FighterValidator} = require("../helpers/validators/fighter-validator");

const requiredFields = ['name', 'power', 'defense'];

const createFighterValid = (req, res, next) => {
    const validator = new FighterValidator(res);
    let newFighter = {...fighter};
    newFighter = permitParams(newFighter, requiredFields);

    newFighter.name = validator.name(req.body.name);
    newFighter.power = validator.power(req.body.power);
    newFighter.health = validator.health(req.body.health);
    newFighter.defense = validator.defense(req.body.defense);

    req.body = newFighter;

    next();
}

const updateFighterValid = (req, res, next) => {
    const validator = new FighterValidator(res);
    const newData = req.body;
    const updatedFighter = {};

    for (let key in newData) {
        if (key in permitParams({...fighter}, requiredFields)) {
            const value = newData[key];
            updatedFighter[key] = validator[key](value);

            if (!updatedFighter[key]) {
                break;
            }
        }
    }

    req.body = updatedFighter;

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
